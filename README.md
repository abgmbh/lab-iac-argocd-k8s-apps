# How to use NGINX App Protect as an ingress controller for Kubernetes

## Overview

The NGINX App Protect (NAP, <https://www.nginx.com/products/nginx-app-protect/> ) can be deployed in a Kubernetes environment and function as an ingress controller (IC) with built-in L7 WAF.

When deploying NAP in your Kubernetes cluster, the installation process is very similar to installing NGINX Plus as an IC, but with additional steps that are specific to NAP.

This repo leverages argoCD ( <https://argo-cd.readthedocs.io/en/stable/> ) as a GitOps continuous delivery tool to show case an end to end example of using NAP to frontend a simple Kubernetes application.

As this repo is public facing, I am also using a tool named 'Sealed-Secrets' to encrypt all the secret manifests. However, this is not a requirement for NAP.

I will go through argoCD and Sealed-Secrets first as supporting pieces and then go into NAP itself.

## argoCD

If you don't know argoCD, I strongly recommend that you check it out. In essense, with argoCD, you create an argoCD application that references a location (e.g., git repo or folder) containing all your manifests. argoCD applies all the manifests in that location and constantly minitors and syncs changes from that location. e.g., if you made a change to a manifest or added a new one, after you did a git commit for that change, argoCD picks it up and immediate applies the change within your Kubernetes.

The following screenshot taken from argoCD shows that I have an app named 'cafe'. The 'cafe' app points to a git repo where all manifests are stored. The status is 'Healthy' and 'Synced'. It means that argoCD has successfully applied all the manifests that it knows about and the manifests that it knows about is in sync with the git repo.

![image info](./files/argoCD-cafe.png)

The `cafe` argoCD application manifest is shown [here](bootstrap/cafe.yaml). For this repo, all argoCD application manifests are stored in the [bootstrap](bootstrap) folder.

To add the Cafe argoCD application, run the following,

```bash
kubectl apply -f cafe.yaml
```

## Sealed Secrets

When Kubernetes manifests that contain secrets such as passwords and private keys, they cannot simply be pushed to a public repo.

With Sealed-secrets ( <https://github.com/bitnami-labs/sealed-secrets#controller> ), you can solve this problem by sealing those manifests files offline via the binary and then push them to the public repo. When you apply the sealed secret manifests from the public repo, the sealed-secrets component that sits inside Kubernetes will decrypt the sealed secrets and then apply them on the fly.

To do this, you must upload the encryption key into Kubernetes first.

Please note that I am only using sealed-secret so I can push my secret manifests to a public repo, for the purpose of this demo. It is not a requirement to install NAP. If you have a private repo, you can simply push all your secret manifests there and argoCD will then apply them as is.

The following commands set up sealed-secret with my specified certificate/key in its own namespace.

```bash
export PRIVATEKEY="dc7.h.l.key"
export PUBLICKEY="dc7.h.l.cer"
export NAMESPACE="sealed-secrets"
export SECRETNAME="dc7.h.l"

kubectl -n "$NAMESPACE" create secret tls "$SECRETNAME" --cert="$PUBLICKEY" --key="$PRIVATEKEY"

kubectl -n "$NAMESPACE" label secret "$SECRETNAME" sealedsecrets.bitnami.com/sealed-secrets-key=active
```

To create a sealed TLS secret,

```bash
kubectl create secret tls wildcard.abbagmbh.de --cert=wildcard.abbagmbh.de.cer --key=wildcard.abbagmbh.de.key -n nginx-ingress --dry-run=client -o yaml | kubeseal \
    --controller-namespace sealed-secrets \
    --format yaml \
    > sealed-wildcard.abbagmbh.de.yaml
```

## NGINX App Protect

The NAP for Kubernetes is an NGINX Plus (NP) ingress controller with L7 WAF capabilities.

The installation process for NAP is largely identical to NP, with the following additional steps.

* Apply NAP specific CRD's to Kubernetes
* Apply NAP log configuration (NAP logging is different from NP)
* Apply NAP protection policy

The official installation docs using manifests ( <https://docs.nginx.com/nginx-ingress-controller/installation/installation-with-manifests/> ) have great info around the entire process. This repo simply collated all the necessary manifests required for NAP in a directory that is then fed to argoCD.

**Image Pull Secret**

With the NAP docker image, you can either pull it from the official nginx private repo, or from your own repo. In the former case, you'd need to create a secret that is generated from the JWT file (part of nginx license files). See below for detail.

To create a sealed docker-registry secret,

```bash
username=`cat nginx-repo.jwt`

kubectl create secret docker-registry private-registry.nginx.com \
--docker-server=private-registry.nginx.com \
--docker-username=$username \
--docker-password=none \
--namespace nginx-ingress \
--dry-run=client -o yaml | kubeseal \
    --controller-namespace sealed-secrets \
    --format yaml \
    > sealed-docker-registry-secret.yaml
```

Notice the controller namespace above, it needs to match the namespace where you installed Sealed-Secrets.

**NAP CRD's**

A number of NAP specific CRD's (Custom Resource Definition) are required for installation. They are included in the [crds](/nap-kic-aks/crds/) directory and should be picked up and applied by argoCD automatically.

**NAP Configuration**

The NAP configuration includes the followings in this demo.

* User defined signature
* App protect policy
* App protect log configuration

This  [user defined signature](/nap-kic-aks/ap-conf/ap-apple-uds.yaml) shows an example of a custom signature that looks for keyword **apple** in requeset traffic.

The [app protect policy](/nap-kic-aks/ap-conf/ap-dataguard-alarm-policy.yaml) defines violation rules. In this case it blocks traffic caught by the custom signature defined above. This sample policy also enables dataguard and all other protection features defined in a base template.

The [app protect log configuration](/nap-kic-aks/ap-conf/ap-logconf.yaml) defines what gets logged and how they look like. e.g., log all traffic versus log illegal traffic.

This repo also includes a manifest for syslog deployment as a log destination used by NAP.

## Ingress

In order to use NAP, you must create an **Ingress** resource.

Within the Ingress manifest, you use annotations to apply NAP specific settings that were discussed above.

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: cafe-ingress
  annotations:
    kubernetes.io/ingress.class: "nginx"
    appprotect.f5.com/app-protect-policy: "nginx-ingress/dataguard-alarm"
    appprotect.f5.com/app-protect-enable: "True"
    appprotect.f5.com/app-protect-security-log-enable: "True"
    appprotect.f5.com/app-protect-security-log: "nginx-ingress/logconf"
    appprotect.f5.com/app-protect-security-log-destination: "syslog:server=syslog-svc.nginx-ingress:514"
```

The traffic routing logic is done via the followings,

```yaml
spec:
  ingressClassName: nginx # use only with k8s version >= 1.18.0
  tls:
  - hosts:
    - cafe.abbagmbh.de
    secretName: wildcard.abbagmbh.de
  rules:
  - host: cafe.abbagmbh.de
    http:
      paths:
      - path: /tea
        pathType: Prefix
        backend:
          service:
            name: tea-svc
            port:
              number: 8080
      - path: /coffee
        pathType: Prefix
        backend:
          service:
            name: coffee-svc
            port:
              number: 8080
```

## Testing

Once you added both applications (in bootstrap folder) into argoCD, you should see the followings in argoCD UI.

![image info](./files/argoCD-cafe-nap.png)

We can do a test to confirm if NAP routes traffic based upon HTTP URI, as well as whether WAF protection is applied.

First get the Ingress Controller IP.

```bash
% kubectl get ingress
NAME           CLASS   HOSTS              ADDRESS          PORTS     AGE
cafe-ingress   nginx   cafe.abbagmbh.de   x.x.x.x   80, 443   1d
```

Now send traffic to both **'/tea'** and **'/coffee'** URI paths.

```bash
% curl --resolve cafe.abbagmbh.de:443:x.x.x.x https://cafe.abbagmbh.de/tea
Server address: 10.244.0.22:8080
Server name: tea-6fb46d899f-spvld
Date: 03/May/2022:06:02:24 +0000
URI: /tea
Request ID: 093ed857d28e160b7417bb4746bec774

% curl --resolve cafe.abbagmbh.de:443:x.x.x.x https://cafe.abbagmbh.de/coffee
Server address: 10.244.0.21:8080
Server name: coffee-6f4b79b975-7fwwk
Date: 03/May/2022:06:03:51 +0000
URI: /coffee
Request ID: 0744417d1e2d59329401ed2189067e40
```

As you can see from above, traffic destined to '/tea' is routed to the tea pod (tea-6fb46d899f-spvld) and traffic destined to '/coffee' is routed to the coffee pod (coffee-6f4b79b975-7fwwk).

Let's trigger a violation based on the user defined signature,

```bash
% curl --resolve cafe.abbagmbh.de:443:x.x.x.x https://cafe.abbagmbh.de/apple

<html><head><title>Request Rejected</title></head><body>The requested URL was rejected. Please consult with your administrator.<br><br>Your support ID is: 10807744421744880061<br><br><a href='javascript:history.back();'>[Go Back]</a></body></html>
```

Finally, traffic violating the XSS rule.

```bash
curl --resolve cafe.abbagmbh.de:443:x.x.x.x 'https://cafe.abbagmbh.de/tea<script>'

<html><head><title>Request Rejected</title></head><body>The requested URL was rejected. Please consult with your administrator.<br><br>Your support ID is: 10807744421744881081<br><br><a href='javascript:history.back();'>[Go Back]</a></body></html>
```

Confirming that logs are received on the syslog pod.

```text
% tail -f /var/log/message

May  3 06:30:32 nginx-ingress-6444787b8-l6fzr ASM:attack_type="Non-browser Client Abuse of Functionality Cross Site Scripting (XSS)"
blocking_exception_reason="N/A"
date_time="2022-05-03 06:30:32"
dest_port="443"
ip_client="x.x.x.x"
is_truncated="false"
method="GET"
policy_name="dataguard-alarm"
protocol="HTTPS"
request_status="blocked"
response_code="0"
severity="Critical"
sig_cves=" "
sig_ids="200000099 200000093"
sig_names="XSS script tag (URI) XSS script tag end (URI)"
sig_set_names="{Cross Site Scripting Signatures;High Accuracy Signatures} {Cross Site Scripting Signatures;High Accuracy Signatures}"
src_port="1478"
sub_violations="N/A"
support_id="10807744421744881591"
threat_campaign_names="N/A"
unit_hostname="nginx-ingress-6444787b8-l6fzr"
uri="/tea<script>"
violation_rating="5"
vs_name="24-cafe.abbagmbh.de:8-/tea"
x_forwarded_for_header_value="N/A"
outcome="REJECTED"
outcome_reason="SECURITY_WAF_VIOLATION"
violations="Illegal meta character in URL
Attack signature detected
Violation Rating Threat detected
Bot Client Detected"
json_log="{violations:[{enforcementState:{isBlocked:false}
violation:{name:VIOL_URL_METACHAR}}
{enforcementState:{isBlocked:true}
violation:{name:VIOL_RATING_THREAT}}
{enforcementState:{isBlocked:true}
violation:{name:VIOL_BOT_CLIENT}}
{enforcementState:{isBlocked:true}
signature:{name:XSS script tag (URI)
signatureId:200000099}
violation:{name:VIOL_ATTACK_SIGNATURE}}
{enforcementState:{isBlocked:true}
signature:{name:XSS script tag end (URI)
signatureId:200000093}
violation:{name:VIOL_ATTACK_SIGNATURE}}]}"
```

## Conclusion

The NGINX App Protect functions both as an Ingress Controller and a built-in WAF for your Kubernetes environment.

I hope that you find the deployment  simple and straightforward.
